/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2018 University of Antwerp
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package be.uantwerpen.algorithms.tiles;

import static com.google.common.collect.Maps.newHashMap;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import be.uantwerpen.exception.InvalidOption;
import be.uantwerpen.patterns.tile.TilePattern;
import be.uantwerpen.utils.SetReporter;
import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.common.IndexSets;
import de.unibonn.realkd.data.propositions.DefaultPropositionalContext;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTransactionFileFactory;

/**
 * Driver class for sampling tiles from transactional databases
 *
 * @author Sandy Moens
 * @since 1.1.0
 * @version 2.0.0
 */
public class RTSDriver {

	private static final String Help = "help";

	private static final String Output = "output";

	private static final String nl = "\n";
	private static final String jump = "\t";

	/**
	 * Main driver function
	 *
	 * @param args
	 *            list of arguments for sampling
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			printHelpMenu();
			return;
		}

		if (Help.startsWith(args[0])) {
			if (args.length == 1) {
				printHelpMenu();
			}
			return;
		}

		Map<String, String> options = newHashMap();
		try {
			parseOptions(Arrays.copyOf(args, args.length - 2), options);
		} catch (InvalidOption e) {
			System.out.println(e.getMessage() + nl);
			printHelpMenu();
			return;
		}

		String fileName = args[args.length - 2];
		Integer samples = Integer.parseInt(args[args.length - 1]);

		System.out.print("Reading dataset...");
		DefaultPropositionalContext context = PropositionalLogicFromTransactionFileFactory.build(fileName, " ");
		System.out.println("Done!");

		printInfo(context, samples);

		SetReporter setReporter = getSetReporter(options.get(Output));

		RecursiveTileSampler rts = new RecursiveTileSampler(context);
		rts.setSetReporter(setReporter);

		Collection<TilePattern> sample = rts.sample(samples);

		for (TilePattern tilePattern : sample) {
			Iterator<Proposition> iterator = tilePattern.descriptor().elements().iterator();
			IndexSet indexSet = iterator.next().supportSet();
			while (iterator.hasNext()) {
				indexSet = IndexSets.intersection(indexSet, iterator.next().supportSet());
			}
		}

		rts.close();
	}

	/**
	 * Prints all information used in the sampling process
	 *
	 * @param context
	 *            the propositional context
	 * @param samples
	 *            the number of samples
	 */
	private static void printInfo(PropositionalContext context, Integer samples) {
		StringBuilder builder = new StringBuilder();
		builder.append("Sampling patterns" + nl);
		builder.append("=================" + nl);
		builder.append("Dataset:         " + context.name() + nl);
		builder.append("#rows:			" + String.format("%d", context.population().size()) + nl);
		builder.append("#propositions:	" + String.format("%d", context.propositions().size()) + nl);
		builder.append("Samples:         " + samples + nl);

		System.out.println(builder.toString());
	}

	/**
	 * Prints the help menu
	 */
	private static void printHelpMenu() {
		StringBuilder builder = new StringBuilder();
		builder.append("Please specify:" + jump + "[options] filename sampleSize" + nl + nl);
		builder.append(jump + "List of options:" + nl);
		// @formatter:off
		addOption(builder, "-o", "specifies the outputfile", "",
				"if no output is specified, results are written to standard output");
		// @formatter:on
		System.out.println(builder.toString());
	}

	/**
	 * Adds new option to the specified StringBuilder
	 *
	 * @param builder
	 *            the builder that is expanded
	 * @param option
	 *            option flag ('-F')
	 * @param comment
	 *            comment on how to use the flag
	 * @param defaultValue
	 *            default value for the option
	 * @param extra
	 *            some extra information
	 */
	private static void addOption(StringBuilder builder, String option, String comment, String defaultValue,
			String extra) {
		builder.append(jump + option + jump + comment + nl);
		if (defaultValue.length() != 0) {
			builder.append(jump + jump + "(default: " + defaultValue + ")" + nl);
		}
		if (extra.length() != 0) {
			builder.append(jump + jump + "(" + extra + ")" + nl);
		}
	}

	/**
	 * Parses the extra options from the command line
	 *
	 * @param args
	 *            command line arguments in the format '-fvalue'
	 * @param options
	 *            map containing parsed options
	 * @throws InvalidOption
	 *             thrown if one of the flags is invalid (i.e. does not start with
	 *             '-'). The rest of the flags is not parsed anymore
	 */
	private static void parseOptions(String[] args, Map<String, String> options) throws InvalidOption {
		for (String arg : args) {
			if (!arg.startsWith("-")) {
				throw new InvalidOption(arg);
			}

			char option = arg.charAt(1);
			String value = arg.substring(2);

			if (option == 'o') {
				options.put(Output, value);
			}
		}
	}

	/**
	 * Gets the set reporter based on the flag value. I.e. if -o has been specified,
	 * the reporter will write to file, otherwise it will write to commandline.
	 *
	 * @param value
	 *            flag value
	 * @return a FileReporter if the value is non null, otherwise a CMDReporter
	 */
	private static SetReporter getSetReporter(String value) {
		if (value == null) {
			return new SetReporter.CMDReporter();
		}
		if (value.length() != 0) {
			return new SetReporter.FileReporter(value);
		}
		return new SetReporter.CMDReporter();
	}
}
