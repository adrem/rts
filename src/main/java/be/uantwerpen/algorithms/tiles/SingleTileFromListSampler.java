/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2018 University of Antwerp
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package be.uantwerpen.algorithms.tiles;

import static com.google.common.collect.Lists.newArrayList;

import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

import be.uantwerpen.patterns.tile.TilePattern;
import be.uantwerpen.utils.Utils;

/**
 *
 * @author Sandy Moens
 * @since 1.1.0
 * @version 2.0.0
 */
public interface SingleTileFromListSampler {

	public static class LargestAreaChooser implements SingleTileFromListSampler {

		private static Random random = new Random();

		@Override
		public TilePattern sampleOne(Collection<TilePattern> tilePatterns) {
			if (tilePatterns.size() == 1) {
				return tilePatterns.iterator().next();
			}
			Iterator<TilePattern> it = tilePatterns.iterator();

			TilePattern maxTile = null;
			int maxSize = -1;

			while (it.hasNext()) {
				TilePattern tilePattern = it.next();
				int size = tilePattern.descriptor().elements().size() * tilePattern.descriptor().supportSet().size();
				if (size > maxSize) {
					maxTile = tilePattern;
					maxSize = size;
				} else if (size == maxSize && random.nextBoolean()) {
					maxTile = tilePattern;
					maxSize = size;
				}
			}
			return maxTile;
		}
	}

	public static class LargestAreaNoSingleChooser implements SingleTileFromListSampler {

		private static Random random = new Random();

		@Override
		public TilePattern sampleOne(Collection<TilePattern> tilePatterns) {
			if (tilePatterns.size() == 1) {
				return tilePatterns.iterator().next();
			}
			Iterator<TilePattern> it = tilePatterns.iterator();

			TilePattern maxTile = null;
			int maxSize = -1;

			while (it.hasNext()) {
				TilePattern tilePattern = it.next();
				int itemSize = tilePattern.descriptor().elements().size();
				int tidsSize = tilePattern.descriptor().supportSet().size();
				int size = itemSize * tidsSize;
				if (itemSize != 1 && tidsSize != 1 && size > maxSize) {
					maxTile = tilePattern;
					maxSize = size;
				} else if (itemSize != 1 && tidsSize != 1 && size == maxSize && random.nextBoolean()) {
					maxTile = tilePattern;
					maxSize = size;
				}
			}
			return maxTile;
		}
	}

	public static class Area2Sampler implements SingleTileFromListSampler {

		private static Random random = new Random();

		@Override
		public TilePattern sampleOne(Collection<TilePattern> tilePatterns) {
			if (tilePatterns.size() == 1) {
				return tilePatterns.iterator().next();
			}
			double[] cumul = new double[tilePatterns.size()];
			int i = 0;
			double tot = 0;
			for (TilePattern tilePattern : tilePatterns) {
				double area = tilePattern.descriptor().elements().size() * tilePattern.descriptor().supportSet().size();
				cumul[i++] = tot += area * area;
			}

			return newArrayList(tilePatterns).get(Utils.logIndexSearch(cumul, random.nextDouble() * tot));
		}
	}

	public TilePattern sampleOne(Collection<TilePattern> tilePatterns);
}
