/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2018 University of Antwerp
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package be.uantwerpen.algorithms.tiles;

import static be.uantwerpen.utils.Utils.logIndexSearch;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Maps.newHashMapWithExpectedSize;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newHashSetWithExpectedSize;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import be.uantwerpen.patterns.tile.TilePattern;
import be.uantwerpen.patterns.tile.Tiles;
import be.uantwerpen.utils.SetReporter;
import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.common.IndexSets;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;

/**
 *
 * @author Sandy Moens
 * @since 1.1.0
 * @version 2.0.0
 */
public class RecursiveTileSampler implements TileSampler {

	public static class CellSample {
		int rowIx;
		int colIx;

		public CellSample(int rowIx, int colIx) {
			this.rowIx = rowIx;
			this.colIx = colIx;
		}
	}

	protected static boolean debug = false;
	protected static boolean steps = false;
	public static Random random = new Random(System.currentTimeMillis());

	public static void main(String[] args) {
	}

	private final PropositionalContext context;

	protected final Set<Integer> currTids;
	protected final Set<Proposition> currItems;
	protected IndexSet tidsLeft;
	protected final Set<Proposition> itemsLeft;
	protected IndexSet fullTids;
	protected final Set<Proposition> fullItems;
	public Map<Proposition, Integer> indexMap;
	public int[] origTidCounts;
	public int[] origItemCounts;
	public int[] tidCounts;
	public int[] itemCounts;
	public double[] cumulColValues;
	public double[] cumulRowValues;
	protected IndexSet discardedTransactions;
	protected Set<Proposition> discardedItems = newHashSet();
	protected IndexSet discardedTr;
	protected Set<Proposition> discardedIt;
	private int nonNull;
	private boolean isFull;
	protected final Set<TilePattern> tilesLastRun;
	private Collection<TilePattern> tiles;
	private long[] timings;
	private long totalSamples = 0;
	private long totalMisses = 0;

	private boolean stop;

	private SingleTileFromListSampler singleTileSampler;
	private SetReporter setSetReporter;

	public RecursiveTileSampler(PropositionalContext context) {
		this.context = context;

		this.singleTileSampler = new SingleTileFromListSampler.LargestAreaChooser();

		this.indexMap = newHashMapWithExpectedSize(this.context.propositions().size());
		int ix = 0;
		for (Proposition proposition : this.context.propositions()) {
			this.indexMap.put(proposition, ix++);
		}

		this.currTids = newHashSet();
		this.currItems = newHashSet();

		this.tidsLeft = null;
		this.itemsLeft = newHashSetWithExpectedSize(this.context.propositions().size());

		this.fullTids = IndexSets.empty();
		this.fullItems = newHashSet();

		this.discardedTransactions = IndexSets.empty();
		this.discardedTr = IndexSets.empty();

		this.discardedItems = newHashSet();
		this.discardedIt = newHashSet();

		this.cumulColValues = new double[context.propositions().size()];
		this.cumulRowValues = new double[context.population().size()];

		this.origItemCounts = new int[context.propositions().size()];
		this.origTidCounts = new int[context.population().size()];

		this.tilesLastRun = newHashSet();

		this.setSetReporter = new SetReporter.NullReporter();

		createOriginalCountArrays();
	}

	public void setSingleTileSampler(SingleTileFromListSampler singleTileSampler) {
		this.singleTileSampler = singleTileSampler;
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	public long getTotalSamples() {
		return this.totalSamples;
	}

	public long getTotalMisses() {
		return this.totalMisses;
	}

	@Override
	public void close() {
		this.setSetReporter.close();
	}

	public void stop() {
		this.stop = true;
	}

	@Override
	public Collection<TilePattern> sample(int numberOfTiles) {
		this.stop = false;
		this.timings = new long[numberOfTiles];
		this.tiles = newLinkedList();
		for (int i = 0; i < numberOfTiles; i++) {
			long beg = System.currentTimeMillis();
			TilePattern tilePattern = runLargeTile();
			this.timings[i] = System.currentTimeMillis() - beg;
			this.tiles.add(tilePattern);
			this.setSetReporter.report(tilePattern);
			if (this.stop) {
				break;
			}
		}

		final Map<String, Proposition> map = Maps.newHashMap();

		this.context.propositions().stream().forEach(p -> map.put(p.name(), p));

		return this.tiles;
	}

	@Override
	public Collection<TilePattern> sampleWithTimeCap(long timeCapInMilliSeconds) {
		this.stop = false;

		List<Long> times = newArrayList();

		this.tiles = newLinkedList();
		long initTime = System.currentTimeMillis();
		while (true) {
			long beg = System.currentTimeMillis();
			TilePattern tilePattern = runLargeTile();
			long timeFromInit = System.currentTimeMillis() - initTime;
			if (timeFromInit > timeCapInMilliSeconds || this.stop) {
				break;
			}
			times.add(System.currentTimeMillis() - beg);
			if (tilePattern.descriptor().elements().size() == 0) {
				continue;
			}
			this.tiles.add(tilePattern);
			this.setSetReporter.report(tilePattern);
		}

		this.timings = new long[times.size()];
		for (int i = 0; i < times.size(); i++) {
			this.timings[i] = times.get(i);
		}

		return this.tiles;
	}

	@Override
	public Collection<TilePattern> sampleWithTimeCap(int k, long timeCapInMilliSeconds) {
		this.stop = false;

		List<Long> times = newArrayList();

		this.tiles = newLinkedList();
		long initTime = System.currentTimeMillis();
		for (int i = 0; i < k; i++) {
			long beg = System.currentTimeMillis();
			TilePattern tilePattern = runLargeTile();
			long timeFromInit = System.currentTimeMillis() - initTime;
			if (timeFromInit > timeCapInMilliSeconds || this.stop) {
				break;
			}
			times.add(System.currentTimeMillis() - beg);
			if (tilePattern.descriptor().elements().size() == 0) {
				continue;
			}
			this.tiles.add(tilePattern);
			this.setSetReporter.report(tilePattern);
		}

		this.timings = new long[times.size()];
		for (int i = 0; i < times.size(); i++) {
			this.timings[i] = times.get(i);
		}

		return this.tiles;
	}

	@Override
	public long[] getTimings() {
		return this.timings;
	}

	public final TilePattern runLargeTile() {
		return this.singleTileSampler.sampleOne(runLargeTiles());
	}

	public Set<TilePattern> runLargeTiles() {
		initialize();

		createCountArrays();
		updateValues();

		if (debug) {
			System.out.println("Initial Db");
			// printValues();
		}

		do {
			// if only ones left in matrix, we can add all immediately
			if (this.isFull) {
				if (debug) {
					System.out.println("+++++++++++++++++++++++ ISFULL ++++++++++++++++++");
					System.out.println("Early abort:");
					System.out.println("From: " + this.currTids.size() + "*" + this.currItems.size() + "="
							+ this.currTids.size() * this.currItems.size() + " (" + this.currItems + ", "
							+ this.currTids + ")");
				}
				this.currItems.addAll(this.itemsLeft);
				for (int ix : this.tidsLeft) {
					this.currTids.add(ix);
				}
				if (debug) {
					System.out.println("To: " + this.currTids.size() + "*" + this.currItems.size() + "="
							+ this.currTids.size() * this.currItems.size());
				}
				break;
			}

			if (this.cumulColValues[this.itemsLeft.size() - 1] == 0
					|| this.cumulRowValues[this.tidsLeft.size() - 1] == 0) {
				break;
			}

			CellSample cellSample = getNextIx();

			updateStatus(cellSample);
			updateCounts();
			updateValues();

			// add horizontal tile
			checkHorizontalTile(this.tilesLastRun);

			// add vertical tile
			checkVerticalTile(this.tilesLastRun);

			debugPrintCurrentStatus();
		} while (canStillExpand() && !this.stop);

		if (this.isFull) {
			this.currItems.addAll(this.itemsLeft);
			for (int ix : this.tidsLeft) {
				this.currTids.add(ix);
			}
			TilePattern createTile = Tiles.tilePattern(this.context, this.currItems, IndexSets.copyOf(this.currTids));
			this.tilesLastRun.add(createTile);
		} else if (this.tidsLeft.size() != 0 && this.itemsLeft.size() != 0) {
			Set<Proposition> items = createClosureUsingTids();
			IndexSet supportSet = createClosureUsingItems();
			TilePattern e = Tiles.tilePattern(this.context, items, supportSet);
			this.tilesLastRun.add(e);
		}
		return this.tilesLastRun;
	}

	private Set<Proposition> createClosureUsingTids() {
		Set<Proposition> items = newHashSet(this.currItems);
		items.addAll(this.itemsLeft);
		for (int ix : this.tidsLeft) {
			items.retainAll(this.context.truthSet(ix).stream().map(i -> this.context.proposition(i))
					.collect(Collectors.toList()));
		}
		return items;
	}

	private IndexSet createClosureUsingItems() {
		IndexSet indexSet = IndexSets.copyOf(this.currTids);
		indexSet = IndexSets.union(indexSet, this.tidsLeft);

		for (Proposition proposition : this.currItems) {
			indexSet = IndexSets.intersection(indexSet, proposition.supportSet());
		}
		for (Proposition proposition : this.itemsLeft) {
			indexSet = IndexSets.intersection(indexSet, proposition.supportSet());
		}

		return indexSet;
	}

	private CellSample getNextIx() {
		int rowIx, colIx;
		int i = 0;
		do {
			double rSearch = random.nextDouble() * this.cumulRowValues[this.tidsLeft.size() - 1];
			double cSearch = random.nextDouble() * this.cumulColValues[this.itemsLeft.size() - 1];
			rowIx = logIndexSearch(this.cumulRowValues, this.tidsLeft.size() - 1, rSearch);
			colIx = logIndexSearch(this.cumulColValues, this.itemsLeft.size() - 1, cSearch);
			i++;
		} while (!isPresent(rowIx, colIx));
		this.totalSamples += i;
		this.totalMisses += i - 1;
		return new CellSample(rowIx, colIx);
	}

	private boolean isPresent(int rowIx, int colIx) {
		int tIx = getTransactionIx(rowIx);
		return getProposition(colIx).supportSet().contains(tIx);
	}

	private void initialize() {
		this.currTids.clear();
		this.currItems.clear();

		this.tidsLeft = this.context.population().objectIds();
		this.itemsLeft.clear();
		this.itemsLeft.addAll(this.context.propositions());

		this.fullTids = IndexSets.empty();
		this.fullItems.clear();

		this.discardedTransactions = IndexSets.empty();
		this.discardedTr = IndexSets.empty();
		this.discardedItems.clear();
		this.discardedIt.clear();

		this.isFull = false;

		this.tilesLastRun.clear();
	}

	private void checkHorizontalTile(Set<TilePattern> tiles) {
		if (this.itemsLeft.isEmpty() && this.fullTids.isEmpty()) {
			return;
		}
		List<Proposition> propositions = newArrayList(this.currItems);
		propositions.addAll(this.itemsLeft);

		IndexSet supportSet = IndexSets.union(this.fullTids, IndexSets.copyOf(this.currTids));

		TilePattern e = Tiles.tilePattern(this.context, propositions, supportSet);
		tiles.add(e);
		// System.out.println(e + " " + e.getTIDs().cardinality() * e.size());
	}

	private void checkVerticalTile(Set<TilePattern> tiles) {
		if (this.fullItems.isEmpty() && this.tidsLeft.isEmpty()) {
			return;
		}
		List<Proposition> propositions = newArrayList(this.currItems);
		propositions.addAll(this.fullItems);

		IndexSet supportSet = IndexSets.union(IndexSets.copyOf(this.currTids), this.tidsLeft);

		TilePattern e = Tiles.tilePattern(this.context, propositions, supportSet);
		tiles.add(e);
		// System.out.println(e + " " + e.getTIDs().cardinality() * e.size());
	}

	private void printValues() {
		System.out.print(String.format("%10s ", 0));
		int c = 0;
		for (Proposition proposition : this.itemsLeft) {
			System.out.print(String.format("%10s ", proposition.name() + " (c=" + c++ + ")"));
		}
		System.out.println();

		for (int rowIx : this.tidsLeft) {
			System.out.print(String.format("%10s ", rowIx));

			int cSize = this.tidCounts[rowIx];

			for (Proposition proposition : this.itemsLeft) {
				int size = 0;
				if (proposition.supportSet().contains(rowIx)) {
					int iSize = this.itemCounts[this.indexMap.get(proposition)];
					size = cSize * iSize;
				}
				System.out.print(String.format("%10s ", size));
			}
			System.out.println();
		}
	}

	private boolean canStillExpand() {
		return this.tidsLeft.size() != 0 && this.itemsLeft.size() != 0 && this.nonNull != 0;
	}

	public void init() {
		initialize();
		createCountArrays();
		updateValues();
		printValues();
	}

	private void updateStatus(CellSample cellSample) {
		int rowIx = cellSample.rowIx;
		int colIx = cellSample.colIx;

		int tIx = getTransactionIx(rowIx);
		Set<Proposition> transaction = this.context.truthSet(tIx).stream().map(i -> this.context.proposition(i))
				.collect(Collectors.toSet());

		Proposition proposition = getProposition(colIx);

		this.currTids.add(tIx);
		this.currItems.add(proposition);

		this.discardedTr = IndexSets.union(IndexSets.difference(this.tidsLeft, proposition.supportSet()),
				IndexSets.copyOf(ImmutableList.of(tIx)));

		// discardedTr.andNot(discardedTransactions);
		// discardedTransactions.or(discardedTr);

		this.discardedIt = setDifference(this.itemsLeft, transaction);
		this.discardedIt.add(proposition);

		// discardedIt.removeAll(discardedItems);
		// discardedItems.addAll(discardedIt);

		this.tidsLeft = IndexSets.difference(IndexSets.intersection(this.tidsLeft, proposition.supportSet()),
				IndexSets.copyOf(ImmutableList.of(tIx)));

		this.itemsLeft.retainAll(transaction);
		this.itemsLeft.remove(proposition);
		if (debug) {
			System.out.println("currTids: " + this.tidsLeft);
			System.out.println("currItems: " + this.itemsLeft);
		}
	}

	private <T> Set<T> setDifference(Set<T> set1, Set<T> set2) {
		Set<T> set = newHashSet();

		for (T t : set1) {
			if (!set2.contains(t)) {
				set.add(t);
			}
		}

		return set;
	}

	private int getTransactionIx(int rowIx) {
		Iterator<Integer> it = this.tidsLeft.iterator();
		for (int c = 0; c < rowIx; c++) {
			it.next();
		}
		return it.next();
	}

	private Proposition getProposition(int colIx) {
		Iterator<Proposition> it = this.itemsLeft.iterator();
		for (int i = 0; i < colIx; i++) {
			it.next();
		}
		return it.next();
	}

	private void createOriginalCountArrays() {
		int ix;
		ix = 0;
		for (int i : this.context.population().objectIds()) {
			this.origTidCounts[ix++] = this.context.truthSet(i).size();
		}
		ix = 0;
		for (Proposition proposition : this.context.propositions()) {
			this.origItemCounts[ix++] = proposition.supportSet().size();
		}
	}

	private void createCountArrays() {
		this.tidCounts = Arrays.copyOf(this.origTidCounts, this.origTidCounts.length);
		this.itemCounts = Arrays.copyOf(this.origItemCounts, this.origItemCounts.length);
	}

	private void updateValues() {
		this.fullTids = IndexSets.empty();
		this.fullItems.clear();

		this.isFull = true;

		this.nonNull = 0;

		int theSize = this.tidsLeft.size() * this.itemsLeft.size();
		if (theSize == 0) {
			return;
		}

		computeFullItems();
		computeFullTids();

		computeValues();
	}

	private void computeValues() {
		computeCumulColValues();
		computeCumulRowValues();

		this.isFull = true;
		this.nonNull = 1;
		for (Proposition proposition : this.itemsLeft) {
			this.isFull &= this.itemCounts[this.indexMap.get(proposition)] == this.tidsLeft.size();
		}
	}

	private void computeCumulColValues() {
		int i = 0;
		double tot = 0;
		for (Proposition proposition : this.itemsLeft) {
			tot += this.itemCounts[this.indexMap.get(proposition)];
			this.cumulColValues[i++] = tot;
		}
	}

	private void computeCumulRowValues() {
		int i = 0;
		double tot = 0;
		for (int ix : this.tidsLeft) {
			tot += this.tidCounts[ix];
			this.cumulRowValues[i++] = tot;
		}
	}

	private void computeFullTids() {
		int size = this.itemsLeft.size();

		List<Integer> fullTids = newLinkedList();

		for (int rowIx : this.tidsLeft) {
			if (this.tidCounts[rowIx] == size) {
				fullTids.add(rowIx);
			}
		}

		this.fullTids = IndexSets.union(this.fullTids, IndexSets.copyOf(fullTids));
	}

	private void computeFullItems() {
		for (Proposition proposition : this.itemsLeft) {
			if (this.itemCounts[this.indexMap.get(proposition)] == this.tidsLeft.size()) {
				this.fullItems.add(proposition);
			}
		}
	}

	private void updateCounts() {
		updateTransactionCounts();
		updateItemCounts();
	}

	private void updateItemCounts() {
		for (Proposition proposition : this.itemsLeft) {
			IndexSet intersection = IndexSets.intersection(this.discardedTr, proposition.supportSet());
			int ix = this.indexMap.get(proposition);
			this.itemCounts[ix] -= intersection.size();
		}
	}

	private void updateTransactionCounts() {
		for (Proposition proposition : this.discardedIt) {
			IndexSet indexSet = IndexSets.intersection(proposition.supportSet(), this.tidsLeft);

			for (int ix : indexSet) {
				this.tidCounts[ix]--;
			}
		}
	}

	private void printCumulValues() {
		printCumulColValues();
		printCumulRowValues();
	}

	private void printCumulColValues() {
		StringBuilder builder = new StringBuilder();
		for (Proposition proposition : this.itemsLeft) {
			builder.append(this.itemCounts[this.indexMap.get(proposition)] + " ");
		}
		System.out.println(builder.toString());
	}

	private void printCumulRowValues() {
		StringBuilder builder = new StringBuilder();
		for (int ix : this.tidsLeft) {
			builder.append(this.tidCounts[ix] + " ");
		}
		System.out.println(builder.toString());
	}

	private void debugPrintCurrentStatus() {
		if (debug) {
			System.out.println("Current Tids");
			System.out.println(this.currTids);
			System.out.println("Current Items");
			System.out.println(this.currItems);
			System.out.println("Left Tids");
			System.out.println(this.tidsLeft);
			System.out.println("Left Items");
			System.out.println(this.itemsLeft);
			System.out.println("Values");
			// printValues();
			System.out.println("CumulValues");
			printCumulValues();
			System.out.println();

			if (steps) {
				try {
					System.in.read();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void setSetReporter(SetReporter setReporter) {
		this.setSetReporter = setReporter;
	}
}