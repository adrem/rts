/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2018 University of Antwerp
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package be.uantwerpen.patterns.tile;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;
import static de.unibonn.realkd.common.IndexSets.intersection;
import static java.util.Objects.hash;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.annotation.JsonCreator;
//import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.constraints.Constraint;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.Propositions;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 * @author Mario Boley
 * @author Sandy Moens
 *
 * @since 0.2.0
 *
 * @version 0.6.0
 *
 */
public class LogicalDescriptors {

	public static LogicalDescriptor create(Population population, Collection<Proposition> elements) {
		return create(population, elements, supportSet(population, elements));
	}

	public static LogicalDescriptor create(Population population, Collection<Proposition> elements,
			IndexSet supportSet) {
		Optional<DataTable> table = elements.stream().filter(p -> p instanceof AttributeBasedProposition<?>)
				.map(p -> (AttributeBasedProposition<?>) p).map(AttributeBasedProposition::table).findFirst();
		if (table.isPresent()) {
			return createDefault(table.get(), elements, supportSet);
		}
		return createSimple(population, elements, supportSet);
	}

	/**
	 * computes the support set of the pattern by forming the intersection of the
	 * support sets of all propositions. Note that in the special case of an empty
	 * description, the support set has to be the complete set of objects
	 */
	private static IndexSet supportSet(Population population, Collection<Proposition> elements) {
		if (elements.isEmpty()) {
			return population.objectIds();
		}

		Iterator<Proposition> iterator = elements.iterator();
		IndexSet result = iterator.next().supportSet();
		while (iterator.hasNext()) {
			result = intersection(result, iterator.next().supportSet());
		}
		return result;
	}

	private static LogicalDescriptor createSimple(Population population, Collection<Proposition> elements,
			IndexSet supportSet) {
		return new SimpleLogicalDescriptor(population, canonicalOrder(elements), supportSet);
	}

	private static LogicalDescriptor createDefault(DataTable table, Collection<Proposition> elements,
			IndexSet supportSet) {
		return new LogicalDescriptorDefaultImplementation(table, canonicalOrder(elements), supportSet);
	}

	private static List<Proposition> canonicalOrder(Collection<Proposition> elements) {
		List<Proposition> orderedElements = new ArrayList<>(elements);
		Collections.sort(orderedElements, Comparator.comparing(Proposition::toString));
		return orderedElements;
	}

	private static class SimpleLogicalDescriptor implements LogicalDescriptor {

		private final IndexSet supportSet;

		private final ImmutableSet<Proposition> elements;

		private final Population population;

		private SimpleLogicalDescriptor(Population population, Collection<Proposition> orderedElements,
				IndexSet supportSet) {
			this.population = population;
			this.elements = ImmutableSet.copyOf(orderedElements);
			this.supportSet = supportSet;
		}

		@Override
		public boolean minimal() {
			for (Proposition p : this.elements) {
				if (generalization(p).supportSet().size() == supportSet().size()) {
					return false;
				}
			}
			return true;
		}

		@Override
		public LogicalDescriptor lexicographicallyLastMinimalGenerator() {
			LogicalDescriptor current = this;
			for (Proposition p : this.elements) {
				LogicalDescriptor generalization = current.generalization(p);
				if (generalization.supportSet().size() == supportSet().size()) {
					current = generalization;
				}
			}
			return current;
		}

		/**
		 *
		 * @return number of contained propositions
		 */
		@Override
		public int size() {
			return this.elements.size();
		}

		@Override
		public boolean isEmpty() {
			return this.elements.isEmpty();
		}

		@Override
		public boolean empiricallyImplies(Proposition p) {
			// if (semanticallyImplies(p)) {
			// semImplCount++;
			// return true;
			// }
			return p.supportSet().containsAll(supportSet());
		}

		@Override
		public List<String> getElementsAsStringList() {
			List<String> descriptionList = new ArrayList<>();
			for (Proposition proposition : elements()) {
				descriptionList.add(proposition.toString());
			}

			return descriptionList;
		}

		@Override
		public LogicalDescriptor specialization(Proposition augmentation) {
			List<Proposition> newElements = new ArrayList<>(elements());
			newElements.add(augmentation);
			// Collections.sort(newElements, PROPOSITION_ORDER);

			IndexSet newSupportSet = intersection(this.supportSet, augmentation.supportSet());
			return create(this.population, newElements, newSupportSet);
		}

		@Override
		public LogicalDescriptor supportPreservingSpecialization(List<Proposition> augmentations) {
			List<Proposition> newElements = new ArrayList<>(elements());
			boolean augmented = false;
			for (Proposition augmentation : augmentations) {
				if (elements().contains(augmentation)) {
					continue;
				}
				if (augmentation.supportSet().containsAll(supportSet())) {
					// if (propositionalLogic.holdsFor(augmentation.getId(),
					// supportSet())) {
					newElements.add(augmentation);
					augmented = true;
				}
			}
			if (!augmented) {
				return this;
			} else {
				// Collections.sort(newElements, PROPOSITION_ORDER);
				return create(this.population, newElements, this.supportSet);
			}
		}

		@Override
		public LogicalDescriptor generalization(Proposition reductionElement) {
			if (!elements().contains(reductionElement)) {
				throw new IllegalArgumentException("reduction element not part of description");
			}
			List<Proposition> newDescription = new ArrayList<>(elements());
			newDescription.remove(reductionElement);
			return create(this.population, newDescription);

		}

		@Override
		@JsonProperty("elements")
		public ImmutableSet<Proposition> elements() {
			return this.elements;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(o instanceof LogicalDescriptor))
				return false;

			LogicalDescriptor other = (LogicalDescriptor) o;

			return (elements().equals(other.elements()));
		}

		@Override
		public int hashCode() {
			return hash(this.elements);
		}

		@Override
		public String toString() {
			return this.elements.toString();
		}

		@Override
		public IndexSet supportSet() {
			return this.supportSet;
		}

		@Override
		public Population population() {
			return this.population;
		}

		@Override
		public SerialForm<LogicalDescriptor> serialForm() {
			List<SerialForm<? extends Proposition>> elements = this.elements.stream().map(e -> e.serialForm())
					.collect(toList());
			return new DefaultLogicalDescriptorSerialForm(this.population.identifier(), elements);
		}

		@Override
		public boolean refersToAttribute(Attribute<?> attribute) {
			for (Proposition proposition : elements()) {
				if (proposition instanceof AttributeBasedProposition
						&& ((AttributeBasedProposition<?>) proposition).attribute() == attribute) {
					return true;
				}
			}

			return false;
		}

		@Override
		public Iterator<Proposition> iterator() {
			return this.elements.iterator();
		}

	}

	/**
	 * <p>
	 * Implementation that buffers support set on creation; this is likely to be
	 * removed in future versions.
	 * </p>
	 */
	private static class LogicalDescriptorDefaultImplementation extends SimpleLogicalDescriptor
			implements TableSubspaceDescriptor {

		private DataTable dataTable;

		private LogicalDescriptorDefaultImplementation(DataTable dataTable, Collection<Proposition> orderedElements,
				IndexSet supportSet) {
			super(dataTable.population(), orderedElements, supportSet);

			this.dataTable = dataTable;
		}

		@Override
		public DataTable table() {
			return this.dataTable;
		}

		@Override
		public List<Attribute<?>> getReferencedAttributes() {
			List<Attribute<?>> result = new ArrayList<>();
			for (Proposition proposition : elements()) {
				if (proposition instanceof AttributeBasedProposition<?>) {
					result.add(((AttributeBasedProposition<?>) proposition).attribute());
				}
			}
			return result;
		}

		@Override
		public SerialForm<LogicalDescriptor> serialForm() {
			Identifier[] attributes = elements().stream().map(e -> (AttributeBasedProposition<?>) e)
					.map(e -> e.attribute().id()).toArray(i -> new Identifier[i]);
			Constraint<?>[] constraints = elements().stream().map(e -> (AttributeBasedProposition<?>) e)
					.map(e -> e.constraint()).toArray(i -> new Constraint<?>[i]);
			return new AttributeBasedLogicalDescriptorSerialForm(this.dataTable.identifier(), attributes, constraints);
		}

	}

	public static class AttributeBasedLogicalDescriptorSerialForm implements SerialForm<LogicalDescriptor> {

		@JsonProperty("table")
		private final String table;

		@JsonProperty("attributes")
		private final Identifier[] attributes;

		@JsonProperty("constraints")
		private final Constraint<?>[] constraints;

		private final ImmutableList<String> dependencies;

		@JsonCreator
		private AttributeBasedLogicalDescriptorSerialForm(@JsonProperty("table") String tableId,
				@JsonProperty("attributes") Identifier[] attributes,
				@JsonProperty("constraints") Constraint<?>[] constraints) {
			this.table = tableId;
			this.attributes = attributes;
			this.constraints = constraints;
			this.dependencies = ImmutableList.of(this.table);
		}

		private <T> AttributeBasedProposition<T> propositionOfIndex(DataTable table, int i) {
			@SuppressWarnings("unchecked")
			Attribute<T> attribute = (Attribute<T>) table.attribute(this.attributes[i]).get();
			@SuppressWarnings("unchecked")
			Constraint<T> constraint = (Constraint<T>) this.constraints[i];
			return Propositions.proposition(table, attribute, constraint);
		}

		@Override
		public LogicalDescriptor build(Workspace workspace) {
			DataTable tableEntity = workspace.get(this.table, DataTable.class).get();
			List<Proposition> props = IntStream.range(0, this.constraints.length)
					.mapToObj(i -> propositionOfIndex(tableEntity, i)).collect(Collectors.toList());
			return createDefault(tableEntity, props, supportSet(tableEntity.population(), props));
		}

		@Override
		public Collection<String> dependencyIds() {
			return this.dependencies;
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof AttributeBasedLogicalDescriptorSerialForm)) {
				return false;
			}
			AttributeBasedLogicalDescriptorSerialForm that = (AttributeBasedLogicalDescriptorSerialForm) other;
			return (this.table.equals(that.table) && Arrays.equals(this.attributes, that.attributes)
					&& Arrays.equals(this.constraints, that.constraints));
		}
	}

	public static SerialForm<LogicalDescriptor> logicalDescriptorBuilder(String propositionalLogicIdentifier) {
		return new DefaultLogicalDescriptorSerialForm(propositionalLogicIdentifier, newArrayList());
	}

	public static class DefaultLogicalDescriptorSerialForm implements SerialForm<LogicalDescriptor> {

		@JsonProperty("population")
		private final String populationId;

		@JsonProperty("elements")
		private final List<SerialForm<? extends Proposition>> elements;

		@JsonCreator
		public DefaultLogicalDescriptorSerialForm(@JsonProperty("population") String populationId,
				@JsonProperty("elements") List<SerialForm<? extends Proposition>> elements) {
			this.populationId = populationId;
			this.elements = elements;
		}

		@Override
		public LogicalDescriptor build(Workspace context) {
			checkArgument(context.contains(this.populationId, Population.class),
					"Workspace does not contain artifact '" + this.populationId + "' of type Population");
			Population populationEntity = context.get(this.populationId, Population.class).get();
			List<Proposition> elements = this.elements.stream().map(e -> e.build(context)).collect(toList());
			return create(populationEntity, elements);
		}

		@Override
		public Collection<String> dependencyIds() {
			return ImmutableSet.of(this.populationId);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof DefaultLogicalDescriptorSerialForm)) {
				return false;
			}
			DefaultLogicalDescriptorSerialForm other = (DefaultLogicalDescriptorSerialForm) obj;
			return this.populationId.equals(other.populationId) && this.elements.equals(other.elements);
		}
	}

	@Deprecated
	public static class LogicalDescriptorBuilderDefaultImplementation implements SerialForm<LogicalDescriptor> {

		private final String propositionalLogicIdentifier;

		private final ArrayList<Integer> elementIndices;

		@JsonCreator
		public LogicalDescriptorBuilderDefaultImplementation(
				@JsonProperty("propositionalLogicIdentifier") String propositionalLogicIdentifier,
				@JsonProperty("elementIndexList") List<Integer> elementIndices) {
			this.propositionalLogicIdentifier = propositionalLogicIdentifier;
			this.elementIndices = Lists.newArrayList(elementIndices);
		}

		@Override
		public LogicalDescriptor build(Workspace context) {
			checkArgument(context.contains(this.propositionalLogicIdentifier, PropositionalContext.class),
					"Workspace does not contain artifact '" + this.propositionalLogicIdentifier
							+ "' of type PropositionalLogic");
			PropositionalContext propositionalLogic = (PropositionalContext) context
					.get(this.propositionalLogicIdentifier);
			List<Proposition> propositions = this.elementIndices.stream()
					.map(i -> propositionalLogic.propositions().get(i)).collect(Collectors.toList());
			return create(propositionalLogic.population(), propositions);
		}

		public List<Integer> getElementIndexList() {
			return this.elementIndices;
		}

		public String getPropositionalLogicIdentifier() {
			return this.propositionalLogicIdentifier;
		}

		@Override
		public Collection<String> dependencyIds() {
			return ImmutableSet.of(getPropositionalLogicIdentifier());
		}

		// public void propositionalLogicIdentifier(String identifier) {
		// this.propositionalLogicIdentifier = identifier;
		// }

		@Override
		public boolean equals(Object other) {
			if (other == this) {
				return true;
			}
			if (!(other instanceof LogicalDescriptorBuilderDefaultImplementation)) {
				return false;
			}
			LogicalDescriptorBuilderDefaultImplementation otherDescriptor = (LogicalDescriptorBuilderDefaultImplementation) other;
			return (this.elementIndices.equals(otherDescriptor.elementIndices)
					&& this.propositionalLogicIdentifier.equals(otherDescriptor.propositionalLogicIdentifier));
		}

	}

}
