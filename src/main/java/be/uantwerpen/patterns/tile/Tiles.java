/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2018 University of Antwerp
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package be.uantwerpen.patterns.tile;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 *
 * @author Sandy Moens
 * @since 2.0.0
 * @version 2.0.0
 */
public class Tiles {

	public static TilePattern tilePattern(PropositionalContext context, Collection<Proposition> propositions,
			IndexSet supportSet) {
		return tilePattern(context.population(),
				LogicalDescriptors.create(context.population(), propositions, supportSet), Lists.newArrayList());
	}

	public static TilePattern tilePattern(Population population, LogicalDescriptor logicalDescriptor,
			List<Measurement> measurements) {
		return new DefaultTilePattern(population, logicalDescriptor, measurements);
	}

	private static class DefaultTilePattern extends DefaultPattern<LogicalDescriptor> implements TilePattern {

		public DefaultTilePattern(Population population, LogicalDescriptor descriptor, List<Measurement> measurements) {
			super(population, descriptor, measurements);
		}

	}

}
