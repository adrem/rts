/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2018 University of Antwerp
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package be.uantwerpen.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import be.uantwerpen.patterns.tile.TilePattern;
import de.unibonn.realkd.data.propositions.Proposition;

public interface SetReporter {

	public void report(TilePattern tilePattern);

	public void close();

	public static class NullReporter implements SetReporter {
		@Override
		public void report(TilePattern tilePattern) {
		}

		@Override
		public void close() {
		}
	}

	public static class CMDReporter implements SetReporter {
		private static final String nl = "\n";
		private static final int BufferSize = 1;

		private final StringBuilder builder;
		private int counter;

		public CMDReporter() {
			this.builder = new StringBuilder();
			this.counter = 0;
		}

		@Override
		public void report(TilePattern tilePattern) {
			for (Proposition proposition : tilePattern.descriptor().elements()) {
				builder.append(proposition.name() + " ");
			}
			builder.setLength(builder.length() - 1);
			builder.append(nl);

			counter++;
			if (counter % BufferSize == 0) {
				System.out.print(builder.toString());
				builder.setLength(0);
			}
		}

		@Override
		public void close() {
			if (builder.length() != 0) {
				System.out.print(builder.toString());
				builder.setLength(0);
			}
		}
	}

	public static class FileReporter implements SetReporter {
		private BufferedWriter writer;

		public FileReporter(String fileName) {
			try {
				this.writer = new BufferedWriter(new FileWriter(fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void report(TilePattern tilePattern) {
			try {
				for (Proposition proposition : tilePattern.descriptor().elements()) {
					writer.append(proposition.name() + " ");
				}
				writer.newLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void close() {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
